﻿from flask import Flask
from flask import render_template
from flask import request

import re  # Подключаем регулярки

import pymorphy2
morph = pymorphy2.MorphAnalyzer()

app = Flask(__name__)

@app.route("/")
@app.route('/index')
def hello():
    return render_template('index.html', title='Home')

@app.route('/handle_data', methods=['POST'])
def handle_data():
    text = request.form['text']

    tokens = re.findall(r'\w+-\w+|\w+', text)  # Разбиваем на токены
    tokensCount = len(tokens)
    print(tokensCount)

    words = {}

    for token in tokens:

        word_info = morph.parse(token)[0]
        if words.get(word_info.normal_form) == None:
            words[word_info.normal_form] = 1
        else:
            words[word_info.normal_form] = words.get(word_info.normal_form) + 1
        # if words.count(word_info.normal_form) == 0:
        #     words.append(word_info.normal_form)
        #
        #     print(token)
        #     print(word_info.normal_form)
        #
        #     if word_info.tag.POS != None:
        #         print(word_info.tag.POS)

    print(words)

    uniqueWords = len(words)
    print(uniqueWords)
    return render_template('index.html', title='Home', tokens=words, text=text, tokensCount=tokensCount, uniqueWords=uniqueWords)


if __name__ == "__main__":
    app.run()