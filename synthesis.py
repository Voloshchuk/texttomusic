import librosa
import numpy as np

filename = {"audio1.mp3", "audio2.mp3"}

# 2. Load the audio as a waveform `y`
#    Store the sampling rate as `sr`
y_1, sr_1 = librosa.load(filename[0])
y_2, sr_2 = librosa.load(filename[1])

print(type(y_1))

size_1 = y_1.size
size_2 = y_2.size

size = size_1 + size_2 - sr_1*5

y = np.zeros((size))

# print(y.size)
print(y_1)
print(y_2)
y = np.hstack((y_1, y_2))

i = 0
j = size_1 - sr_1 * 5
k = 0


while i < size:
    if i < j:
        y[i] = y_1[i]

    elif i >= j and i < size_1:
        y[i] = y_1[j+k]/2 + y_2[k]

        k += 1

    else:
        y[i] = y_2[k]
        k += 1
    i += 1

print(y)

librosa.output.write_wav("output.wav", y, sr_1)
